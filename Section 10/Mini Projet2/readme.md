---------------------------------------------------------------------------------------------
			**Partie 1 : Extraction des données ==> Fichiers Excel vers CSV**   
				  [Vidéo de formation](https://youtu.be/r-u14RFMTTs)
---------------------------------------------------------------------------------------------
Récupération du fichier Excel : SAISIE_DEPENSE.xlsx (Emplacement : Section 10\Mini Projet2\Partie 1)

	
*Liste des composants utilisés ainsi que leurs familles d'appartenance:*
	Orchestration : tPrejob, tPostjob, tFileList, tFlowToIterate, tRunJob
	Log & Errors  : tChronometerStart, tChronometerStop, tLogRow
	Custom Code   : tJava
	File          : tFileInputExcel, tFileOutputDelimited, tFileOutputExcel
	Processing    : tMap
	Misc          : tFixedFlowInput



---------------------------------------------------------------------------------------------
		**Partie 2 : Intégration des données csv dans l'Operational Data Store (ODS)**
					[Vidéo de formation](https://youtu.be/5k6wHMQChXs)
---------------------------------------------------------------------------------------------

**Étape de développement 1 : Base de données  ==> PostgreSQL** 
    
Exécution des scripts SQL 
Répertoire GIT à utiliser : scriptSQL
*Dossier 1- Initialize*
	
	1-script_create_database.sql
		Création de la base de données IDEPENSE_INGESTION_DB (Vous pouvez directement créer la base de données à partir de l'interface pgAdmin 4)

	2-script_create_schema_params.sql
		Création du schéma PARAMS_LOG

	3-script_create_table_contexte.sql
		Création de la table CONTEXTE

	4-script_insert_context_variable_depense.sql
		Insertion des données dans la table CONTEXTE 
		NB : Veuillez modifier les paramètres de connexion à la base de données (le port et le mot de passe).
	
	
*Dossier 2- ODS*
	1-script_create_schema_ods.sql 
		Création du schéma DEPENSE_ODS 

	2-script_create_table_ods.sql
		Création des différentes tables ODS

	3-script_insert_context_variable_ods.sql
		Insertion des données dans la table CONTEXTE 

	4-script_truncate_table_ods.sql
		Ce script permet de supprimer les données présentes dans les différentes tables ods.

	5-script_select_table_ods.sql
		Ce script permet de sélectionner les données insérées dans les différentes tables ods.

   
**Étape de développement 2 : Répertoire de données du jour**
Répertoire GIT à utiliser : Files\csvFiles

	1- Création du répertoire contenant les données : C:\PROJET_TALEND\IDEPENSE_REPORTING\DATA

	2- Dépôt des répertoires du jour contenant les fichiers plats.            
		Exemple : 20230921
		NB : Pour faire vos tests, vous devez avoir le répetoire du jour du test.
			 Exemple : Si vous faites vos développements le 21/09/2023, vous devez avoir un répertoire du jour nommé : 20230921
			 Nous avons mis à votre disposition trois fichiers plats. Les fichiers plats sont dans le dossier : Section 10\Mini Projet2\Partie 2\Files\csvFiles
					1- IDEP_20230921_CATEGORIE.csv
					2- IDEP_20230921_SOUS_CATEGORIE.csv
					3- IDEP_20230921_DEPENSE.csv
			 Veuillez les récupérer et mettre cela dans le répertoire adapté afin de tester vos jobs.
			 Aussi, veuillez changer la date qui se trouve dans le nom des fichiers et mettre la date du jour, toujours au format YYYYMMDD.
	 


**Étape de développement 3 : Processus d’intégration de données ==> Talend**

	1- Ouverture / Création  du projet Talend : IDEPENSE_REPORTING
	
	2- Création des groupes de contexte 
	
	3- Création des métadonnées : Connexion à la base de données
	
	4- Configuration du chargement implicite des variables de contexte
	
	5- Configuration d’insertion des Stats et Logs dans les tables de la BDD
	
	6- Création des différents schémas génériques : Fichiers plats
	
	7- Création des job ODS (Transformation et Chargement de la données)
	
	8- Création du job principal 
	
	9- Construction et planification de l’exécution du job principal 


NB : Vous trouverez dans le répertoire *Section 10\Mini Projet2\Partie 2\Files* les fichiers ci-après : 
	codeJavaUseInJobODS.java : Les codes java utilisés dans les composants tJava
	customCode.zip : Routine utilisée. Il faut importer cette routine.
	
	
*Liste des composants utilisés ainsi que leurs familles d'appartenance:*
	Orchestration : tPrejob, tPostjob, tFileList, tRunJob
	Misc          : tContextLoad
	Log & Errors  : tChronometerStart, tChronometerStop, tDie, tLogRow
	Database      : tDBConnection, tDBCommit, tDBClose, tDBInput, tDBOutput
	Custom Code   : tSetGlobalVar, tJava
	File          : tFileInputDelimited, tFileExist
	Processing    : tMap, tUniqRow
	

	
---------------------------------------------------------------------------------------------
		**Partie 3 : Intégration des données de l'ODS dans le Data WareHouse (DWH)**
					[Vidéo de formation](https://youtu.be/I_UUHChzb8E)
---------------------------------------------------------------------------------------------	

**Étape de développement 1 : Base de données  ==> PostgreSQL** 
    
Exécution des scripts SQL 
Répertoire GIT à utiliser : scriptSQL

	1-script_create_schema_dwh.sql 
		Création du schéma DEPENSE_DWH 

	2-script_create_table_dwh.sql
		Création des différentes tables DWH
		
	3-script_select_table_dwh.sql
		Ce script permet de sélectionner les données insérées dans les différentes tables DWH.
		
	4-script_truncate_table_dwh.sql
		Ce script permet de supprimer les données présentes dans les différentes tables DWH.


**Étape de développement 2 : Processus d’intégration de données ==> Talend**

	1- Ouverture du projet Talend : IDEPENSE_REPORTING
	
	2- Création d'un groupe de contexte 
	
	3- Création de la métadonnées : Connexion à la base de données
	
	4- Création des job DWH
	
	5- Création du job principal 
	
	6- Construction et planification de l’exécution du job principal 


NB : Vous trouverez dans le répertoire *Section 10\Mini Projet2\Partie 3* les fichiers ci-après : 
	codeJavaUseInJobDWH.java : Les codes java utilisés dans les composants tJava
	jGenerateCalendar.zip : Job Talend permettant d'alimenter la table DIM_CALENDRIER. Il faut importer ce job.
	joursFeries.xlsx : Fichier Excel contenant les jours fériés à utiliser dans le job jGenerateCalendar
	
*Liste des composants utilisés ainsi que leurs familles d'appartenance:*
	Orchestration : tPrejob, tPostjob, tRunJob
	Misc          : tContextLoad
	Log & Errors  : tChronometerStart, tChronometerStop
	Database      : tDBConnection, tDBCommit, tDBClose, tDBInput, tDBOutput
	Custom Code   : tJava
	Processing    : tMap
	 